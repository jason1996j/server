import { SuperTest, Test } from "supertest"
import { createUser } from "../../src/database"
import { AuthRole } from "../../src/entities/User"

export class GqlRequest {
  request: SuperTest<Test>

  constructor(request: SuperTest<Test>) {
    this.request = request
  }

  async gqlRequest(query: string, resCode: number = 200) {
    return await this.request
      .post('/graphql')
      .send({ query })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(resCode)
  }

  async protectedGqlRequest(query: string, resCode: number = 200, role: AuthRole = AuthRole.USER) {
    const email = 'test@test.test'
    const password = 'test'
  
    const user = await createUser({
      email,
      password,
      role
    })
  
    const res1 = await this.gqlRequest(`query {
      login(input: {
        email: "${email}",
        password: "${password}"
      }) {
        token
      }
    }`)
  
    if (!res1.ok || !res1.body.data.login.token) {
      throw new Error('login failed')
    }
  
    const res2 = await this.request
      .post('/graphql')
      .send({ query })
      .set('Accept', 'application/json')
      .set('access_token', `Bearer ${res1.body.data.login.token}`)
      .expect('Content-Type', /json/)
      .expect(resCode)

    return {
      res: res2,
      user
    }
  }
}
