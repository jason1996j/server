import { mongoose } from "@typegoose/typegoose"
import supertest from "supertest"
import { connectToDatabase, createUser } from "../src/database"
import { makeServer } from "../src/server"
import { GqlRequest } from "./helpers/request"

let request: GqlRequest

beforeAll(async done => {
  await connectToDatabase('testAuth')
  await mongoose.connection.db.dropDatabase()

  const server = await makeServer()

  request = new GqlRequest(supertest(server))

  done()
})

afterEach(async done => {
  await mongoose.connection.db.dropDatabase()

  done()
})

afterAll(async done => {
  await mongoose.disconnect()

  done()
})

describe('login', () => {
  test('expected', async done => {
    const email = 'test@email.com'
    const password = 'testPass'

    await createUser({
      email,
      password
    })

    const res = await request.gqlRequest(`query {
      login(input: {
        email: "${email}",
        password: "${password}"
      }) {
        token
      }
    }`, 200)

    expect(res.body.data.login.token).not.toBeNull()

    done()
  })

  test('invalid-email', async done => {
    const email = 'test@email.com'
    const password = 'testPass'

    await createUser({
      email,
      password
    })

    const res = await request.gqlRequest(`query {
      login(input: {
        email: "t@email.com",
        password: "${password}"
      }) {
        token
      }
    }`, 200)

    expect(res.body.data.login).toBeNull()

    done()
  })

  test('invalid-password', async done => {
    const email = 'test@email.com'
    const password = 'testPass'

    await createUser({
      email,
      password
    })

    const res = await request.gqlRequest(`query {
      login(input: {
        email: "${email}",
        password: "pass"
      }) {
        token
      }
    }`, 200)

    expect(res.body.data.login).toBeNull()

    done()
  })
})

describe('renew', () => {
  test('expected', async done => {
    const { res } = await request.protectedGqlRequest(`query {
      renew {
        token
        version
      }
    }`)

    expect(res.body.data.renew.token).not.toBeNull()
    expect(res.body.data.renew.version).toBe(1)

    done()
  })

  test('unauthorized', async done => {
    const res = await request.gqlRequest(`query {
      renew {
        token
        version
      }
    }`)

    expect(res.body.data.renew).toBeNull()

    done()
  })
})

describe('logout', () => {
  test('expected', async done => {
    const { res } = await request.protectedGqlRequest(`query {
      logout
    }`)

    expect(res.ok).toBe(true)
    expect(res.body.data.logout).toBe(true)

    done()
  })

  test('unauthorized', async done => {
    const res = await request.gqlRequest(`query {
      logout
    }`)

    expect(res.body.data).toBeNull()

    done()
  })
})