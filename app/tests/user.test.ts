import { DocumentType } from "@typegoose/typegoose"
import mongoose  from "mongoose"
import { stringify } from "querystring"
import supertest from "supertest"
import { connectToDatabase, createTrip, createUser } from "../src/database"
import { Trip } from "../src/entities/Trip"
import { AuthRole, User, UserModel } from "../src/entities/User"
import { makeServer } from "../src/server"
import { GqlRequest } from "./helpers/request"

let request: GqlRequest

beforeAll(async done => {
  await connectToDatabase('testUser')
  await mongoose.connection.db.dropDatabase()

  const server = await makeServer()

  request = new GqlRequest(supertest(server))

  done()
})

afterEach(async done => {
  await mongoose.connection.db.dropDatabase()

  done()
})

afterAll(async done => {
  await mongoose.disconnect()

  done()
})

describe('createUser', () => {
  test('expected', async done => {
    const email = 'test@email.com'
    const password = 'testPass'

    const res = await request.gqlRequest(`mutation {
      createUser(input: {
        email: "${email}",
        password: "${password}"
      }) {
        _id
        email 
      } 
    }`)

    const user = UserModel.findById(res.body.data.createUser._id)

    expect(user).not.toBeNull()
    expect(res.body.data.createUser.email).toBe(email)

    done()
  })

  test('duplicate', async done => {
    const email = 'test@email.com'
    const password = 'testPass'

    await request.gqlRequest(`mutation {
      createUser(input: {
        email: "${email}",
        password: "${password}"
      }) {
        email 
      } 
    }`)

    const res = await request.gqlRequest(`mutation {
      createUser(input: {
        email: "${email}",
        password: "${password}"
      }) {
        email 
      } 
    }`)

    expect(res.body.errors).toHaveLength(1)

    done()
  })

  test('missing-field-email', async done => {
    const password = 'testPass'

    await request.gqlRequest(`mutation {
      createUser(input: {
        password: "${password}"
      }) {
        email 
      } 
    }`, 400)

    done()
  })

  test('missing-field-password', async done => {
    const email = 'test@email.com'

    await request.gqlRequest(`mutation {
      createUser(input: {
        email: "${email}"
      }) {
        email 
      } 
    }`, 400)

    done()
  })
})

describe('user', () => {
  test('expected', async done => {
    const user = await createUser({
      email: 'q@email.com',
      password: 'q'
    })

    for (let i = 0; i < 12; i++) {
      const trip = await createTrip({
        name: `trip${i}`,
        creator: user.id,
        start: new Date()
      })

      user.allTrips.push(trip.id)
    }

    await user.save()

    const numTrips = 8
    const tripOffset = 2

    const { res } = await request.protectedGqlRequest(`query {
      user(id: "${user._id}") {
        email
        trips(input: { number: ${numTrips}, offset: ${tripOffset} }) {
          items {
            name
            start
          }
          hasMore
        }
      }
    }`, 200)

    expect(res.body.data.user.email).toBe(user.email)
    expect(res.body.data.user.trips.items).toHaveLength(numTrips)
    expect(res.body.data.user.trips.items[0].name).toBe(`trip${tripOffset}`)
    expect(res.body.data.user.trips.hasMore).toBeTruthy()

    done()
  })

  test('unauthorized', async done => {
    const user = await createUser({
      email: 'q@email.com',
      password: 'q'
    })

    const res = await request.gqlRequest(`query {
      user(id: "${user._id}") {
        email
      }
    }`)

    expect(res.body.data.user).toBeNull()

    done()
  })

  test('invalid-id', async done => {
    const res = await request.gqlRequest(`query {
      user(id: "test") {
        email
      }
    }`, 400)

    expect(res.body.errors).not.toBeNull()

    done()
  })
})

describe('me', () => {
  test('expected', async done => {
    const { res, user } = await request.protectedGqlRequest(`query {
      self {
        email
      }
    }`)

    expect(res.body.data.self.email).toBe(user.email)

    done()
  })

  test('unauthorized', async done => {
    const user = await createUser({
      email: 'test@email.com',
      password: 'test'
    })

    const res = await request.gqlRequest(`query {
      self {
        email
      }
    }`)

    expect(res.body.error).not.toBeNull()

    done()
  })
})
