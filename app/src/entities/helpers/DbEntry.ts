import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses"
import { ObjectId } from "mongodb"
import { ObjectType, Field } from "type-graphql"

export interface IDbEntry {
  readonly _id: ObjectId
  readonly createdAt: Date
  readonly updatedAt: Date
}

@ObjectType()
export class DbEntry extends TimeStamps {
  @Field()
  readonly _id!: ObjectId

  @Field()
  readonly createdAt!: Date

  @Field()
  readonly updatedAt!: Date
}