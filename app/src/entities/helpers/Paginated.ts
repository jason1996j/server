import { Ref } from "@typegoose/typegoose";
import { Max, Min } from "class-validator";
import { ClassType, Field, InputType, Int, ObjectType } from "type-graphql";

@InputType()
export class PaginatedRequestInput {
  @Field(type => Int, { nullable: true, defaultValue: 0 })
  @Min(0)
  offset!: number

  @Field(type => Int, { nullable: true, defaultValue: 10 })
  @Min(0)
  @Max(10)
  number!: number
}

export function PaginatedResponse<TItemsFieldValue>(cb: (type: undefined | void) => [ClassType<TItemsFieldValue>]) {
  @ObjectType({ isAbstract: true })
  abstract class PaginatedResponseClass {
    @Field(cb, { nullable: 'items' })
    items!: Ref<TItemsFieldValue>[]

    @Field()
    hasMore!: boolean

    // maybe convert to cursor and return new cursor to this position
  }

  return PaginatedResponseClass
}