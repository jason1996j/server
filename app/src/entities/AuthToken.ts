import { Field, Int, ObjectType } from "type-graphql";

export interface IAuthToken {
  readonly token: string
  readonly version: number
}

@ObjectType()
export class AuthToken implements IAuthToken {
  @Field()
  token!: string

  @Field(type => Int)
  version!: number
}