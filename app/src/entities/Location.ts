import { IsLatitude } from "class-validator";
import { Field, Float, ObjectType } from "type-graphql";
import { getModelForClass, prop as Property, Ref } from "@typegoose/typegoose";
import { IUser, User } from "./User";
import { DbEntry, IDbEntry } from "./helpers/DbEntry";

export interface ILocation extends IDbEntry {
  creator: Ref<IUser>
  name: string
  latitude: number
  longitude: number
  imageUrls: string[]
}

@ObjectType()
export class Location extends DbEntry implements ILocation {
  @Field(type => User)
  @Property({ ref: 'User', required: true })
  creator!: Ref<User>

  @Field()
  @Property({ required: true })
  name!: string

  @Field(type => Float)
  @Property({ required: true })
  @IsLatitude()
  latitude!: number

  @Field(type => Float)
  @Property({ required: true })
  @IsLatitude() 
  longitude!: number

  @Field(type => [String], { nullable: 'items' })
  @Property({ type: () => [String], required: true, default: [] })
  imageUrls!: string[]
}

export const LocationModel = getModelForClass(Location)