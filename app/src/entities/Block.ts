import { getModelForClass, prop as Property, Ref } from "@typegoose/typegoose";
import { IsCurrency } from "class-validator";
import { Field, Float, Int, ObjectType } from "type-graphql";
import { DbEntry, IDbEntry } from "./helpers/DbEntry";
import { ILocation, Location } from "./Location";
import { IUser, User } from "./User";

export interface IBlock extends IDbEntry {
  name: string
  creator: Ref<IUser>
  description: string
  thumbsUp: number
  thumbsDown: number
  cost?: number
  start: Ref<ILocation>
  stop?: Ref<ILocation>
  duration: number
}

@ObjectType()
export class Block extends DbEntry implements IBlock {
  @Field()
  @Property({ required: true })
  name!: string

  @Field(type => User)
  @Property({ ref: 'User', required: true })
  creator!: Ref<IUser>

  @Field()
  @Property({ required: true })
  description!: string

  @Field(type => Int)
  @Property({ default: 0 })
  thumbsUp!: number

  @Field(type => Int)
  @Property({ default: 0 })
  thumbsDown!: number

  @Field(type => Float, { nullable: true })
  @Property()
  @IsCurrency()
  cost?: number

  @Field(type => Location)
  @Property({ ref: 'Location', required: true })
  start!: Ref<ILocation>

  @Field(type => Location, { nullable: true })
  @Property({ ref: 'Location' })
  stop?: Ref<ILocation>

  @Field(type => Float)
  @Property({ required: true })
  duration!: number
}

export const BlockModel = getModelForClass(Block)