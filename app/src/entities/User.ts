import { IsEmail } from "class-validator";
import { Field, ObjectType, registerEnumType } from "type-graphql";
import { ITrip, Trip } from "./Trip";
import { getModelForClass, pre, prop as Property, Ref } from "@typegoose/typegoose";
import { DbEntry, IDbEntry } from "./helpers/DbEntry";
import { hash } from "bcryptjs";
import { PaginatedResponse } from "./helpers/Paginated";

// find out how to get mongoose to 
export enum AuthRole {
  USER = 'USER',
  ADMIN = 'ADMIN'
}

registerEnumType(AuthRole, {
  name: 'AuthRole',
  description: 'Describes level of access for creating/editing items'
})

@ObjectType()
class TripsResponse extends PaginatedResponse(type => [Trip]) {
  
}

@ObjectType()
class FriendResponse extends PaginatedResponse(type => [User]) {

}

export interface IUser extends IDbEntry {
  trips: TripsResponse
  allTrips: Ref<ITrip>[]
  friends: FriendResponse
  allFriends: Ref<IUser>[]
  email: string
  password: string
  tokenVersion: number
  role: AuthRole
}

@pre<User>('save', async function() {
  if (this.isModified('password')) {
    this.password = await hash(this.password, 10)
  }
})

@ObjectType()
export class User extends DbEntry implements IUser {
  @Field(type => TripsResponse)
  trips!: TripsResponse

  @Field(type => FriendResponse)
  friends!: FriendResponse

  @Field()
  @Property({ required: true })
  @IsEmail()
  email!: string

  @Field(type => AuthRole)
  @Property({ type: String, enum: AuthRole, required: true, default: AuthRole.USER })
  role!: AuthRole

  @Property({ ref: 'Trip', required: true, default: [] })
  allTrips!: Ref<ITrip>[]

  @Property({ ref: 'User', required: true, default: [] })
  allFriends!: Ref<IUser>[]

  @Property({ required: true })
  password!: string

  @Property({ default: 0 })
  tokenVersion!: number
}

export const UserModel = getModelForClass(User)