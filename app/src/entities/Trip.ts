import { getModelForClass, prop as Property, Ref } from "@typegoose/typegoose";
import { Field, ObjectType } from "type-graphql";
import { Block, IBlock } from "./Block";
import { DbEntry, IDbEntry } from "./helpers/DbEntry";
import { PaginatedResponse } from "./helpers/Paginated";
import { IUser, User } from "./User";

@ObjectType()
class MembersResponse extends PaginatedResponse(type => [User]) {

}

@ObjectType()
class BlocksResponse extends PaginatedResponse(type => [Block]) {
  
}

export interface ITrip extends IDbEntry {
  name: string
  creator: Ref<IUser>
  members: MembersResponse
  allMembers: Ref<IUser>[]
  blocks: BlocksResponse
  allBlocks: Ref<IBlock>[]
  start: Date
}

@ObjectType()
export class Trip extends DbEntry implements ITrip {
  @Field()
  @Property({ required: true })
  name!: string

  @Field(type => User)
  @Property({ ref: 'User', required: true })
  creator!: Ref<IUser>

  @Field(type => MembersResponse)
  members!: MembersResponse

  @Field(type => BlocksResponse)
  blocks!: BlocksResponse

  @Field()
  @Property({ required: true })
  start!: Date

  @Property({ ref: 'User', required: true, default: [] })
  allMembers!: Ref<IUser>[]

  @Property({ ref: 'Block', required: true, default: [] })
  allBlocks!: Ref<IBlock>[]
}

export const TripModel = getModelForClass(Trip)