import { IsEmail } from "class-validator"
import { ObjectId } from "mongodb"
import { Arg, Authorized, Ctx, Field, FieldResolver, InputType, Mutation, Query, Resolver, ResolverInterface, Root } from "type-graphql"
import { Context } from '../server'
import { IUser, User, UserModel } from "../entities/User"
import { PaginatedRequestInput } from "../entities/helpers/Paginated"

@InputType()
class CreateUserInput implements Partial<IUser> {
  @Field()
  @IsEmail()
  email!: string

  @Field()
  password!: string
}

@Resolver(of => User)
export class UserResolver implements ResolverInterface<IUser> {
  @Authorized()
  @Query(returns => User, { nullable: true, description: 'Find a user by id' })
  async user(@Arg('id') id: ObjectId) {
    return await UserModel.findById(id)
  }

  @Authorized()
  @Query(returns => User, { description: 'Get details of currently logged in user' })
  self(@Ctx() { user }: Context) {
    return user
  }

  @Mutation(returns => User, { description: 'create a new user' })
  async createUser(@Arg('input') { email, password }: CreateUserInput) {
    const existingUsers = await UserModel.find({ email })

    if (existingUsers.length !== 0) {
      throw new Error('Email is already taken')
    }

    const user = new UserModel({
      password,
      email
    })      

    return await user.save()
  }

  @FieldResolver()
  async trips(@Root() { _id }: IUser, @Arg('input') { offset, number }: PaginatedRequestInput) {
    const user = await UserModel.findById(_id)

    if (!user) {
      throw new Error(`user (${_id}) not found`)
    }

    if (!user.allTrips.length) {
      throw new Error('user has no trips')
    }

    const hasMore = offset + number < user.allTrips.length

    user.allTrips = user.allTrips.slice(offset, offset + number)

    await user.populate('allTrips').execPopulate()

    if (!user.populated('allTrips')) {
      throw new Error('unable to populate trips')
    }

    return {
      items: user.allTrips,
      hasMore
    }
  }

  @FieldResolver()
  async friends(@Root() { _id }: IUser, @Arg('input') { offset, number }: PaginatedRequestInput) {
    const user = await UserModel.findById(_id)

    if (!user) {
      throw new Error(`user (${_id}) not found`)
    }

    if (!user.allFriends.length) {
      throw new Error('user has no trips')
    }

    const hasMore = offset + number < user.allFriends.length

    user.allFriends = user.allFriends.slice(offset, offset + number)

    await user.populate('allFriends').execPopulate()

    if (!user.populated('allFriends')) {
      throw new Error('unable to populate friends')
    }

    return {
      items: user.allFriends,
      hasMore
    }
  }
} 
