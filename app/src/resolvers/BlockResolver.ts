import { IsCurrency } from "class-validator";
import { ObjectId } from "mongodb";
import { Arg, Authorized, Field, FieldResolver, Float, InputType, Mutation, Query, Resolver, ResolverInterface, Root } from "type-graphql";
import { Block, BlockModel, IBlock } from "../entities/Block";

@InputType()
class NewBlockInput implements Partial<IBlock> {
  @Field()
  name!: string

  @Field()
  description!: string

  @Field(type => Float, { nullable: true })
  @IsCurrency()
  cost?: number

  @Field()
  start!: ObjectId

  @Field({ nullable: true })
  stop?: ObjectId

  @Field(type => Float)
  duration!: number
}

@Resolver(of => Block)
export class BlockResolver implements ResolverInterface<Block> {
  @Authorized()
  @Query(returns => Block, { nullable: true, description: 'find a block by id' })
  async block(@Arg('id') id: ObjectId) {
    return await BlockModel.findById(id)
  }

  @Authorized()
  @Mutation(returns => Block, { description: 'create a new block' })
  async createBlock(@Arg('input') input: NewBlockInput) {
    const block = new BlockModel(input)

    return await block.save()
  }

  @FieldResolver()
  async creator(@Root() { _id }: IBlock) {
    const block = await BlockModel.findById(_id)

    if (!block) {
      throw new Error(`block (${_id}) not found`)
    }

    await block.populate('creator').execPopulate()

    if (!block.populated('creator')) {
      throw new Error('unable to populate creator')
    }

    return block.creator
  }

  @FieldResolver()
  async start(@Root() { _id }: IBlock) {
    const block = await BlockModel.findById(_id)

    if (!block) {
      throw new Error(`block (${_id}) not found`)
    }

    await block.populate('start').execPopulate()

    if (!block.populated('start')) {
      throw new Error('unable to populate start')
    }

    return block.start
  }

  @FieldResolver()
  async stop(@Root() { _id }: IBlock) {
    const block = await BlockModel.findById(_id)

    if (!block) {
      throw new Error(`block (${_id}) not found`)
    }

    await block.populate('stop').execPopulate()

    if (!block.populated('stop')) {
      throw new Error('unable to populate stop')
    }

    return block.stop
  }
}
