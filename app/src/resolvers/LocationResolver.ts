import { IsLatitude } from "class-validator";
import { ObjectId } from "mongodb";
import { Arg, Authorized, Field, FieldResolver, Float, InputType, Mutation, Query, Resolver, ResolverInterface, Root } from "type-graphql";
import { ILocation, Location, LocationModel } from "../entities/Location";

@InputType()
class NewLocationInput {
  @Field()
  name!: string

  @Field(type => Float)
  @IsLatitude()
  latitude!: number

  @Field(type => Float)
  @IsLatitude()
  longitude!: number

  @Field(type => [String])
  imageUrls!: string[]
}

@Resolver(of => Location)
export class LocationResolver implements ResolverInterface<Location> {
  @Authorized()
  @Query(returns => Location, { nullable: true, description: 'find a location by id' })
  async location(@Arg('id') id: ObjectId) {
    return await LocationModel.findById(id)
  }

  @Authorized()
  @Mutation(returns => Location, { description: 'create a new location' })
  async createLocation(@Arg('input') input: NewLocationInput) {
    const location = new LocationModel(input)

    return await location.save()
  }

  @FieldResolver()
  async creator(@Root() { _id }: ILocation) {
    const location = await LocationModel.findById(_id)
    
    if (!location) {
      throw new Error(`location (${_id}) not found`)
    }

    await location.populate('creator').execPopulate()

    if (!location.populated('creator')) {
      throw new Error('unable to populate creator')
    }

    return location.creator
  }
}
