import { compare, hash } from "bcryptjs";
import { IsEmail } from "class-validator";
import { Arg, Authorized, Ctx, Field, InputType, Query, Resolver, ResolverInterface } from "type-graphql";
import { AuthToken } from "../entities/AuthToken";
import { IUser, UserModel } from "../entities/User";
import { sign } from "../jwt";
import { Context } from "../server";

@InputType()
class LoginInput implements Partial<IUser> {
  @Field()
  @IsEmail()
  email!: string

  @Field()
  password!: string
}

@Resolver(of => AuthToken)
export class AuthTokenResolver {
  @Query(returns => AuthToken, { nullable: true, description: 'get AuthToken with correct credentials' })
  async login(@Arg('input') { email, password }: LoginInput) {
    const user = await UserModel.findOne({ email })

    if (!user) {
      throw new Error('invalid login')
    }

    const isValid = await compare(password, user.password)

    if (!isValid) {
      throw new Error('invalid login')
    }

    const token = await sign({ id: user._id, version: user.tokenVersion })

    return { 
      version: user.tokenVersion,
      token
    }
  }

  @Authorized()
  @Query(returns => AuthToken, { nullable: true, description: 'get new AuthToken if currently logged in, invalidates old tokens' })
  async renew(@Ctx() { user }: Context) {
    if (!user) {
      throw new Error('user not in context')
    }

    user.tokenVersion += 1

    await user.save()

    const token = await sign({ id: user._id, version: user.tokenVersion })

    return {
      version: user.tokenVersion,
      token
    }
  }

  @Authorized()
  @Query(returns => Boolean, { description: 'invalidate all old tokens' })
  async logout(@Ctx() { user }: Context) {
    if (!user) {
      throw new Error('user not in context')
    }

    user.tokenVersion += 1

    await user.save()

    return true
  }
}