import { ObjectId } from "mongodb";
import { Arg, Authorized, Field, FieldResolver, InputType, Mutation, Query, Resolver, ResolverInterface, Root } from "type-graphql";
import { PaginatedRequestInput } from "../entities/helpers/Paginated";
import { ITrip, Trip, TripModel } from "../entities/Trip";

@InputType()
class NewTripInput {
  @Field()
  name!: string

  @Field(type => [ObjectId])
  members!: ObjectId[]

  @Field(type => [ObjectId])
  blocks!: ObjectId[]

  @Field()
  start!: Date
}

@Resolver(of => Trip)
export class TripResolver implements ResolverInterface<Trip> {
  @Authorized()
  @Query(returns => Trip, { nullable: true, description: 'Find a trip by id' })
  async trip(@Arg('id') id: ObjectId) {
    return await TripModel.findById(id)
  }

  @Authorized()
  @Mutation(returns => Trip, { description: 'Create a new trip' })
  async createTrip(@Arg('input') { name, members, blocks, start }: NewTripInput) {
    const trip = new TripModel({
      allBlocks: blocks,
      allMembers: members,
      name,
      start
    })

    return await trip.save()
  }

  @FieldResolver()
  async creator(@Root() { _id }: ITrip) {
    const trip = await TripModel.findById(_id)

    if (!trip) {
      throw new Error(`trip (${_id}) not found`)
    }

    await trip.populate('creator').execPopulate()

    if (!trip.populated('creator')) {
      throw new Error('unable to populate creator')
    }

    return trip.creator
  }

  @FieldResolver()
  async members(@Root() { _id }: ITrip, @Arg('input') { offset, number }: PaginatedRequestInput) {
    const trip = await TripModel.findById(_id)

    if (!trip) {
      throw new Error(`trip (${_id}) not found`)
    }

    if (!trip.allMembers.length) {
      throw new Error('trip has no members')
    }

    const hasMore = offset + number < trip.allMembers.length

    trip.allMembers = trip.allMembers.slice(offset, offset + number)

    await trip.populate('allMembers').execPopulate()

    if (!trip.populated('allMembers')) {
      throw new Error('unable to populate members')
    }

    return {
      items: trip.allMembers,
      hasMore
    }
  }

  @FieldResolver()
  async blocks(@Root() { _id }: ITrip, @Arg('input') { offset, number }: PaginatedRequestInput) {
    const trip = await TripModel.findById(_id)

    if (!trip) {
      throw new Error(`trip (${_id}) not found`)
    }

    if (!trip.allBlocks.length) {
      throw new Error('trip has no blocks')
    }

    const hasMore = offset + number < trip.allBlocks.length

    trip.allBlocks = trip.allBlocks.slice(offset, offset + number)

    await trip.populate('allBlocks').execPopulate()

    if (!trip.populated('allBlocks')) {
      throw new Error('unable to populate blocks')
    }

    return {
      items: trip.allBlocks,
      hasMore
    }
  }
}
