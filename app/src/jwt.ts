import jwt from 'jsonwebtoken'
import { ObjectId } from 'mongodb'

export interface IPayload {
  id: ObjectId
  version: number
}

export const sign = (payload: IPayload, options: jwt.SignOptions = { expiresIn: '1d' }) => {
  return new Promise<string>((resolve, reject) => {
    jwt.sign(payload, 'secret', options, (err, token) => {
      if (err) {
        reject(err)
      } else {
        resolve(token)
      }
    })
  })
}

export const verify = (token: string, options: jwt.VerifyOptions = {}) => {
  return new Promise<IPayload>((resolve, reject) => {
    jwt.verify(token, 'secret', options, (err, decoded) => {
      if (err) {
        reject(err)
      } else {
        resolve(decoded as IPayload)
      }
    })
  })
}
