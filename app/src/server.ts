import { ApolloServer } from "apollo-server-express"
import express from 'express'
import { GraphQLScalarType, Kind } from "graphql"
import { ObjectId } from "mongodb"
import path from "path"
import { AuthChecker, buildSchema } from "type-graphql"
import { IUser, User, UserModel } from "./entities/User"
import { verify } from "./jwt"
import { BlockResolver } from "./resolvers/BlockResolver"
import { LocationResolver } from "./resolvers/LocationResolver"
import { TripResolver } from "./resolvers/TripResolver"
import { UserResolver } from "./resolvers/UserResolver"
import { DocumentType } from '@typegoose/typegoose'
import { AuthTokenResolver } from "./resolvers/AuthTokenResolver"

export interface Context {
  user?: DocumentType<IUser>
}

export const makeServer = async (defaultContext?: Context) => {
  const ObjectIdScalar = new GraphQLScalarType({
    name: "ObjectId",
    description: "MongoDb ObjectId scalar type",
    parseValue: (value: string) => new ObjectId(value),
    serialize: (value: ObjectId) => value.toHexString(),
    parseLiteral: (ast) => ast.kind === Kind.STRING ? new ObjectId(ast.value) : null
  })

  const authChecker: AuthChecker<Context> = ({ context: { user } }, roles) => {
    if (!user) {
      return false
    }

    if (roles.length === 0) {
      return true
    }
    
    return roles.includes(user.role)
  }

  const schema = await buildSchema({
    authChecker,
    resolvers: [
      AuthTokenResolver,
      BlockResolver,
      LocationResolver,
      TripResolver,
      UserResolver
    ],
    emitSchemaFile: path.resolve(__dirname, 'schema.gql'),
    scalarsMap: [{ type: ObjectId, scalar: ObjectIdScalar }]
  })

  const server = new ApolloServer({ 
    schema, 
    playground: true, // turn off when actually in production
    introspection: true, // turn off when actually in production
    context: async ({ req }) => {
      if (defaultContext) {
        // for development testing only
        return defaultContext
      }

      const rawToken = req.headers.access_token

      if (!rawToken || rawToken instanceof Array) {
        return {}
      }

      const accessToken = rawToken.split(' ')[1]

      if (!accessToken) {
        return {}
      }

      let decoded

      try {
        decoded = await verify(accessToken)
      } catch (e) {
        console.log(e)
      }

      if (!decoded) {
        return {}
      }

      const user = await UserModel.findById(decoded.id)

      if (user && user.tokenVersion !== decoded.version) {
        return {}
      }
      
      return { user }
    }
  })

  const app = express()

  server.applyMiddleware({ app })

  return app 
}

