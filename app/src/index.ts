import { makeServer } from './server'
import { connectToDatabase, seedDb } from './database'

(async () => {
  try {
    const mongoose = await connectToDatabase()

    await mongoose.connection.db.dropDatabase() // clean up old db

    const defaultUser = await seedDb() // add fake db info

    const server = await makeServer({ user: defaultUser })
  
    await server.listen(process.env.PORT)
  
    console.log('Server ready')
  } catch (e) {
    console.error(e)
  }
})()