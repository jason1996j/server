import { ObjectId } from "mongodb"
import { connect } from "mongoose"
import { Block, BlockModel } from "./entities/Block"
import { LocationModel } from "./entities/Location"
import { Trip, TripModel } from "./entities/Trip"
import { AuthRole, User, UserModel } from "./entities/User"
import { DocumentType } from '@typegoose/typegoose'

export const createUser = (details: { email: string, password: string, role?: AuthRole }) => {
  const user = new UserModel(details)
  return user.save() as Promise<DocumentType<User>>
}

export const createTrip = (details : { name: string, creator: ObjectId, start: Date }) => {
  const trip = new TripModel(details)
  return trip.save() as Promise<DocumentType<Trip>>
}

export const createLocation = (details : { name: string, creator: ObjectId, latitude: number, longitude: number }) => {
  const location = new LocationModel(details)
  return location.save() as Promise<DocumentType<Location>>
}

export const createBlock = (details : { name: string, creator: ObjectId, description: string, start: ObjectId, duration: number }) => {
  const block = new BlockModel(details)
  return block.save() as Promise<DocumentType<Block>>
}

export const seedDb = async () => {
  const user = await createUser({
    email: 'user1@test.com',
    password: 'user1Pass'
  })

  const friend = await createUser({
    email: 'friend@friend.com',
    password: 'friendPass'
  })

  user.allFriends.push(friend)

  const defaultLocation = await createLocation({
    name: 'location1',
    creator: user._id,
    latitude: 44.986656,
    longitude: -93.258133
  })

  for (let i = 0; i < 21; i++) {
    const trip = await createTrip({
      name: `trip${i}`,
      creator: user._id,
      start: new Date()
    })

    for (let j = 0; j < 15; j++) {
      const block = await createBlock({
        name: `block${i}-${j}`,
        creator: user._id,
        description: 'block 1 desc',
        start: defaultLocation._id,
        duration: 1
      })

      trip.allBlocks.push(block)
    }

    trip.allMembers.push(friend)

    await trip.save()

    user.allTrips.push(trip)
  }

  return user.save() as Promise<DocumentType<User>>
}

export const connectToDatabase = async (dbName: string = 'test') => {
  return connect('mongodb://mongo:27017', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    user: process.env.MONGO_INITDB_ROOT_USERNAME,
    pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
    dbName,
  })
}