FROM nginx:alpine

COPY ./nginx /etc/nginx/conf.d

EXPOSE 80 443

CMD [ "nginx", "-g", "daemon off;" ]