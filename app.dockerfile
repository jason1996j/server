FROM node:alpine as build 

WORKDIR /usr/src/app

COPY ./app .

ENV NODE_ENV production

RUN yarn install --silent && yarn build

RUN yarn add pm2

EXPOSE 4000

CMD [ "npx", "pm2-runtime", "build/index.js" ]